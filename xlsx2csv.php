<?
    require_once('PHPExcel/Classes/PHPExcel.php');

    function convertXLStoCSV($infile, $outfile)
    {
        $fileType = PHPExcel_IOFactory::identify($infile);
        $objReader = PHPExcel_IOFactory::createReader($fileType);

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($infile);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save($outfile);
    }

    $input_name = $_POST['input_name'];
    $base_name = explode('.', $input_name);
    $parts = explode('/', $base_name[0]);
    $file_name = end($parts);
    $output_name = 'csv/' . $file_name . '.' . 'csv';

    convertXLStoCSV($input_name, $output_name);
?>