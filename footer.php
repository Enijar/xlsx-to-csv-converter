                <? if(!isset($_COOKIE['popup'])) { ?>
                <div id="popup-container">
                    <div id="popup-center">
                        <div id="popup-content">
                            <div id="close"> × </div>

                            <div class="half top medgrey-bg xl-font offwhite">
                                XLSX 2 CSV Converter
                                <p class="subtitle m-font offwhite">Convert Excel Files into CSV Files</p>
                            </div>

                            <div class="half bottom l-font">
                                Simply drag and drop files to convert
                            </div>
                        </div>
                    </div>
                </div>
                <? } ?>

            </div> <!-- #container -->
		</div> <!-- #wrapper -->

        <script src="<? echo $config['site']['root']; ?>/js/global.js"></script>
	</body>
</html>