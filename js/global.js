$(function() {
    $('#close').on('click', function() {
        var now = new Date();
        var time = now.getTime();
        time += 3600 * 2419200000;
        now.setTime(time);

        $('#popup-container').fadeOut(350);
        document.cookie = 'popup=false;expires='+ now.toUTCString() +';path=/';
    });
});