<?
	session_start();
	
	require_once 'functions.php';

    if(!isset($_COOKIE['analytics'])) {
        store_tracking_stats();
        setcookie('analytics', true, time() + 2419200, '/');
    }
?>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
	
		<title><? echo $page_title; ?></title>
		<link rel="stylesheet" href="<? echo $config['site']['root']; ?>/css/global.css">
		<script src="<? echo $config['site']['root']; ?>/js/jquery.min.js"></script>
	</head>
	
	<body>
	
		<div id="wrapper">
			<div id="container">