<?
    // Note: Errors are outputted to logs/errors.log in production
	
    $protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https://' : 'http://';
    $host = $_SERVER['HTTP_HOST'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $host_name = gethostname();
    $referrer = $_SERVER['HTTP_REFERER'];
    $date = date('d-m-Y, H:i:s', time());

    if($host === 'molly.local') {
        $config = array(
            'pdo' => array(
                'dsn' => 'mysql:dbname=xlsx2csv;host=localhost',
                'username' => 'root',
                'password' => 'pass1'
            ),
            'site' => array(
                'root' => $protocol . $host . '/2014/xlsx2csv',
                'media' => $protocol . $host . '/2014/xlsx2csv/media',
                'uploads' => $protocol . $host . '/2014/xlsx2csv/uploads'
            ),
            'debug' => true
        );
    } else {
        $config = array(
            'pdo' => array(
                'dsn' => 'mysql:dbname=xlsx2csv;host=localhost',
                'username' => 'root',
                'password' => '07596731069'
            ),
            'site' => array(
                'root' => $protocol . $host . '/labs/xlsx2csv',
                'media' => $protocol . $host . '/labs/xlsx2csv/media',
                'uploads' => $protocol . $host . '/labs/xlsx2csv/uploads'
            ),
            'debug' => false
        );
    }

    if($config['debug'] === true) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        try {
            $db = new PDO($config['pdo']['dsn'], $config['pdo']['username'], $config['pdo']['password']);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    } else {
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        ini_set('error_log', 'logs/errors.log');

        try {
            $db = new PDO($config['pdo']['dsn'], $config['pdo']['username'], $config['pdo']['password']);
        } catch(Exception $e) {
            error_log($e->getMessage(), 0);
        }
    }
?>